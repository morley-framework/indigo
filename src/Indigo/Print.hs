-- SPDX-FileCopyrightText: 2022 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

-- | Module containing pretty-printing of Indigo contracts

module Indigo.Print
  ( printIndigoContract
  , renderIndigoDoc

  , printAsMichelson
  , saveAsMichelson
  , printDocumentation
  , saveDocumentation
  ) where

import Data.Text.Lazy.IO.Utf8 (writeFile)

import Indigo.Common.Object
import Indigo.Compilation
import Indigo.Frontend.Program (IndigoContract)
import Indigo.Lorentz
import Indigo.Prelude

-- | Pretty-print an Indigo contract into Michelson code.
printIndigoContract
  :: forall param st .
     ( IsObject st
     , NiceParameterFull param
     , NiceStorageFull st
     )
  => Bool -- ^ Force result to be single line
  -> CommentSettings
  -> IndigoContract param st
  -> LText
printIndigoContract forceSingleLine sett ctr = printLorentzContract forceSingleLine $
  mkContract $
  compileIndigoContractFull @param @st sett ctr

-- | Generate an Indigo contract documentation.
renderIndigoDoc
  :: forall param st .
     ( IsObject st
     , NiceParameterFull param
     , NiceStorageFull st
     )
  => IndigoContract param st
  -> LText
renderIndigoDoc ctr =
  buildMarkdownDoc . attachDocCommons DGitRevisionUnknown . mkContract $
    compileIndigoContract @param @st ctr

-- | Prints the pretty-printed Michelson code of an Indigo contract to
-- the standard output.
--
-- This is intended to be easy to use for newcomers.
printAsMichelson
  :: forall param st m . ( IsObject st
     , NiceParameterFull param, NiceStorageFull st
     , MonadIO m
     )
  => CommentSettings
  -> IndigoContract param st
  -> m ()
printAsMichelson sett cntr = putStrLn (printIndigoContract @param @st False sett cntr)

-- | Saves the pretty-printed Michelson code of an Indigo contract to
-- the given file.
--
-- This is intended to be easy to use for newcomers.
saveAsMichelson
  :: forall param st m . ( IsObject st
     , NiceParameterFull param, NiceStorageFull st
     , MonadIO m, MonadMask m
     )
  => CommentSettings
  -> IndigoContract param st
  -> FilePath
  -> m ()
saveAsMichelson sett cntr filePath =
  writeFile filePath (printIndigoContract @param @st False sett cntr)

-- | Print the generated documentation to the standard output.
printDocumentation
  :: forall param st m .
     ( IsObject st
     , NiceParameterFull param
     , NiceStorageFull st
     , MonadIO m
     )
  => IndigoContract param st
  -> m ()
printDocumentation ctr =
  putStrLn $ renderIndigoDoc @param @st ctr

-- | Save the generated documentation to the given file.
saveDocumentation
  :: forall param st m .
     ( IsObject st
     , NiceParameterFull param
     , NiceStorageFull st
     , MonadIO m, MonadMask m
     )
  => IndigoContract param st
  -> FilePath
  -> m ()
saveDocumentation ctr filePath = do
  writeFile filePath (renderIndigoDoc @param @st ctr)
