-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

-- | Indigo compiler front-end.
--
-- For reference, "front-end" refers to the part of the compiler pipeline
-- that comes before the intermediate representation. In our case, intermediate
-- representation is defined in "Indigo.Frontend.Internal.Statement".
--
-- This module exports the syntactical constructs of the Indigo language,
-- along with the required types.
module Indigo.Frontend
  ( module ReExports
  ) where

import Indigo.Frontend.Expr as ReExports
import Indigo.Frontend.Language as ReExports
import Indigo.Frontend.Program as ReExports
