-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

-- | This module is intended to be imported instead of @morley-prelude@ by
-- Backend Indigo modules.
--
-- This only serves the purpose of listing @hiding@ rules once and avoid boilerplate.

module Indigo.Backend.Prelude
  ( module Prelude
  ) where

import Prelude hiding ((>>))
