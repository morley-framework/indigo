-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

-- | This module serves the purpose of listing @hiding@ rules of
-- Prelude that conflicts with Indigo exported functions.

module Indigo.Prelude
  ( module Prelude
  ) where

import Prelude hiding
  (abs, and, concat, div, empty, even, fromInteger, fst, get, mod, not, odd, or, snd, some, unless,
  when, whenLeft, whenRight, xor, (%~), (&&), (*), (+), (-), (/), (/=), (<), (<=), (<>), (==), (>),
  (>=), (?:), (^), (^.), (^?), (||))
