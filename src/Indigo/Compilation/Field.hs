-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

module Indigo.Compilation.Field
  ( optimizeFields
  ) where

import Indigo.Backend.Prelude

import Indigo.Common.Var (RefId)
import Indigo.Compilation.Sequential

optimizeFields :: (Block, RefId) -> (Block, RefId)
optimizeFields = id -- TODO #279
