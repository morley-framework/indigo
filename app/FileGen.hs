-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

-- | File Generation machinery that resembles @summoner@ library.
module FileGen
  ( run
  ) where

import FileGen.Tree (FsTree(..), drawFsTree, makeFS)
import System.Directory (setCurrentDirectory)

import FileGen.Files qualified as F
import Paths_indigo (version)

run :: Text -> Maybe Text -> IO ()
run projectName gitRev =
  createProjectDirectoryCustom (CustomSettings projectName gitRev)

-- | From the given 'CustomSettings' creates the project.
createProjectDirectoryCustom :: CustomSettings -> IO ()
createProjectDirectoryCustom settings@CustomSettings{..} = do
  let tree = createProjectTemplateCustom settings
  makeFS "." tree
  putTextLn "The project with the following structure has been created:"
  drawFsTree tree
  setCurrentDirectory (toString sProjectName)

-- | Creating tree structure of the project.
createProjectTemplateCustom :: CustomSettings -> FsTree
createProjectTemplateCustom settings@CustomSettings{..} =
  Dir (toString sProjectName) $ generateFiles settings

-- | Files Template
generateFiles :: CustomSettings -> [FsTree]
generateFiles CustomSettings{..} =
  [ Dir "app"
      [ File "Main.hs" F.main ]
  , Dir "src"
      [ File "Basic.hs" F.basic ]
  , Dir "test"
      [ Dir "Test"
          [ File "Basic.hs" F.basicTest ]
      , File "Main.hs" F.mainTest
      , File "Tree.hs" F.treeTest
      ]
  , File "ChangeLog.md" (F.changelog sProjectName)
  , File "README.md" (F.readme sProjectName)
  , File ".gitignore" F.gitignore
  , File "package.yaml" (F.packageYaml sProjectName)
  , File "stack.yaml" F.stackYaml
  , File "indigo-snapshot.yaml" F.indigoSnapshotYaml
  , File "indigo-dependence-snapshot.yaml" $
      F.indigoDependenceSnapshotYaml $
        maybe (F.DSVPublishedVersion version) F.DSVGitCommitSha sGitRev
  ]

data CustomSettings = CustomSettings
  { sProjectName :: Text
  , sGitRev :: Maybe Text
  }
