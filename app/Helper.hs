-- SPDX-FileCopyrightText: 2020 Tocqueville Group
-- SPDX-FileCopyrightText: 2015-2020, Stack contributors
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ
-- SPDX-License-Identifier: LicenseRef-MIT-Stack-contributors

-- | Indigo Executable Helper functions
module Helper
  ( parsePackageName
  , useUnderscore
  , indigoTitle
  , indigoDesc
  , indigoRunCommand
  , repo
  , doesExistProjectName
  ) where

import Data.Text qualified as T
import Network.HTTP.Req (Scheme(Https), Url, https, (/:))
import Prelude hiding (many)
import System.Directory (doesPathExist, getCurrentDirectory)
import System.FilePath ((</>))
import Text.Megaparsec (Parsec, eof, many, runParser, sepBy1)
import Text.Megaparsec.Char (char, digitChar, letterChar)

-------------------------------------------------------
-- Shared Text
-------------------------------------------------------

indigoTitle :: Text
indigoTitle = "Indigo CLI"

indigoDesc :: Text
indigoDesc = "Indigo CLI provides commands for development and interaction with Indigo project."

indigoRunCommand :: Text
indigoRunCommand = "indigo run"

-------------------------------------------------------
-- Helper
-------------------------------------------------------

type Parser = Parsec Void Text

packageNameParser :: Parser Text
packageNameParser = (toText . intercalate "-")
    <$> sepBy1 word (char '-')
  where
    word = concat <$> sequence [many digitChar,
                                fmap pure letterChar,
                                many (letterChar <|> digitChar)]

-- | Ensure package name is correct.
-- Base on packageParser from `stack`: https://github.com/commercialhaskell/stack/blob/99ecb78b20072cc8cf8a9f8c5bcd6ed95cd4e122/src/Stack/Types/PackageName.hs
parsePackageName :: Text -> Either Text Text
parsePackageName s =
  case runParser (packageNameParser <* eof) "" s of
    Right a -> Right a
    Left _ -> Left $ unlines
      [ "Expected valid package name, but got: " <> s
      , "Package names consist of one or more alphanumeric words separated by hyphens."
      , "To avoid ambiguity with version numbers, each of these words must contain at least one letter."
      ]

-- | Replace dash with underscore in a word.
-- Mainly use to manipulate package name.
useUnderscore :: Text -> Text
useUnderscore = T.replace "-" "_"

repo :: Text -> Url 'Https
repo revision = https "gitlab.com" /: "morley-framework" /: "indigo" /: "-"
  /: "raw" /: revision

doesExistProjectName :: Text -> IO Bool
doesExistProjectName projectName = do
  curPath <- getCurrentDirectory
  doesPathExist $ curPath </> toString projectName
