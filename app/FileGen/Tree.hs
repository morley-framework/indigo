-- SPDX-FileCopyrightText: 2022 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

{-# LANGUAGE ViewPatterns #-}

module FileGen.Tree
       ( FsTree (..)
       , makeFS
       , drawFsTree
       ) where

import Data.Tree (Tree(..))
import Data.Tree.View (drawTree)
import System.Directory (createDirectoryIfMissing)
import System.FilePath ((</>))
import Universum.Lifted.File (writeFile)

-- | Represents a filesystem directory tree description, with 'Text' files.
data FsTree
  = Dir -- ^ A directory
      String -- ^ The name of the directory
      [FsTree] -- ^ The forest of directory contents
  | File -- ^ A text file
      String -- ^ The name of the file
      Text -- ^ File content

-- | Create files and directories from 'FsTree' description.
makeFS
  :: FilePath -- ^ Path to the root directory of the tree
  -> FsTree -- ^ The tree description
  -> IO ()
makeFS root = \case
  File name content -> do
    writeFile (root </> name) content
  Dir name sub -> do
    let path = root </> name
    createDirectoryIfMissing True path
    mapM_ (makeFS path) sub

-- | Output the unicode-art representation of 'FsTree' to stdout.
drawFsTree :: FsTree -> IO ()
drawFsTree = drawTree . toStringTree
  where
    toStringTree = \case
      File name _ -> Node name []
      Dir name sub -> Node name $ toStringTree <$> sub
