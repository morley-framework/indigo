-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

-- | List of functions that generate the files for 'FileGen'.
module FileGen.Files
  ( main
  , basic
  , mainTest
  , treeTest
  , basicTest
  , changelog
  , readme
  , gitignore
  , packageYaml
  , stackYaml
  , indigoSnapshotYaml
  , indigoDependenceSnapshotYaml
  , DependenceSnapshotVersion(..)
  ) where

import Data.FileEmbed (embedStringFile)
import Data.String.Interpolate (i)
import Data.Time.Clock (getCurrentTime)
import Data.Time.Format (defaultTimeLocale, formatTime)
import Data.Version (Version, showVersion)
import System.IO.Unsafe (unsafePerformIO)

import Helper

main :: Text
main = [i|module Main
  ( main
  ) where

import Universum
import qualified Data.Map as Map
import qualified Options.Applicative as Opt
import Options.Applicative.Help.Pretty (Doc, linebreak)
import Main.Utf8 (withUtf8)
import Lorentz (DGitRevision(..))
import Lorentz.ContractRegistry
import System.Environment (withProgName)

import qualified Basic

programInfo :: DGitRevision -> Opt.ParserInfo CmdLnArgs
programInfo gitRev = Opt.info (Opt.helper <*> argParser contracts gitRev) $
  mconcat
  [ Opt.fullDesc
  , Opt.progDesc "#{indigoDesc}"
  , Opt.header "#{indigoTitle}"
  , Opt.footerDoc $ Just usageDoc
  ]

usageDoc :: Doc
usageDoc = mconcat
   [ "You can use help for specific COMMAND", linebreak
   , "EXAMPLE:", linebreak
   , "  #{indigoRunCommand} print --help", linebreak
   ]

contracts :: ContractRegistry
contracts = ContractRegistry $ Map.fromList
  [ "Basic" ?:: ContractInfo
    { ciContract = Basic.basicContractLorentz
    , ciIsDocumented = True
    , ciStorageParser = Just (pure Basic.emptyStorage)
    , ciStorageNotes = Nothing
    }
  ]

main :: IO ()
main = withUtf8 $ withProgName "#{indigoRunCommand}" $ do
  cmdLnArgs <- Opt.execParser (programInfo DGitRevisionUnknown)
  runContractRegistry contracts cmdLnArgs `catchAny` (die . displayException)
|]

basic :: Text
basic = [i|module Basic
  ( basicContractLorentz
  , emptyStorage
  ) where

import Indigo
import Lorentz.Run (Contract)

basicContractLorentz :: Contract Integer Integer ()
basicContractLorentz = mkContract $ compileIndigoContract basicContract

basicContract :: IndigoContract Integer Integer
basicContract param = defContract do
  storage += param

emptyStorage :: Integer
emptyStorage = 0 int

storage :: HasStorage Integer => Var Integer
storage = storageVar
|]

basicTest :: Text
basicTest = [i|module Test.Basic
  ( test_updates_storage_properly
  ) where

import Basic (basicContractLorentz)

import Test.Tasty (TestTree)

import Test.Cleveland

test_updates_storage_properly :: TestTree
test_updates_storage_properly = testScenario "Basic test" $ scenario do
  contractAddr <- originate "basic" (10 :: Integer) basicContractLorentz

  transfer contractAddr $ calling def 90

  getStorage contractAddr @@== 100

|]

treeTest :: Text
treeTest =
  "{-# OPTIONS_GHC -F -pgmF tasty-discover -optF --tree-display -optF \
  \--generated-module -optF Tree #-}\n"

mainTest :: Text
mainTest = [i|module Main (main) where

import Test.Cleveland.Tasty
import Tree (tests)

main :: IO ()
main = tests >>= clevelandMain
|]

changelog :: Text -> Text
changelog projectName = [i|\# Changelog for #{projectName}

\#\# Unreleased changes
|]

readme :: Text -> Text
readme projectName = [i|\# #{projectName}
|]

gitignore :: Text
gitignore = unlines
  [ ".stack-work/"
  , "*~"
  ]

packageYaml :: Text -> Text
packageYaml projectName = [i|name:                #{projectName}
version:             0.1.0.0
github:              "githubuser/#{projectName}"
author:              "Author name here"
maintainer:          "example@example.com"
copyright:           "2020 Author name here"

extra-source-files:
- README.md
- ChangeLog.md

description:         Please see the README on GitHub at <https://github.com/githubuser/#{projectName}\#readme>

verbatim:
  cabal-version: 2.2

default-extensions:
  - AllowAmbiguousTypes
  - BangPatterns
  - BlockArguments
  - ConstraintKinds
  - DataKinds
  - DefaultSignatures
  - DeriveAnyClass
  - DeriveDataTypeable
  - DeriveFoldable
  - DeriveFunctor
  - DeriveGeneric
  - DeriveTraversable
  - DerivingStrategies
  - DerivingVia
  - EmptyCase
  - FlexibleContexts
  - FlexibleInstances
  - GADTs
  - GeneralizedNewtypeDeriving
  - LambdaCase
  - MultiParamTypeClasses
  - MultiWayIf
  - NamedFieldPuns
  - NegativeLiterals
  - NumDecimals
  - OverloadedLabels
  - OverloadedStrings
  - PatternSynonyms
  - PolyKinds
  - QuasiQuotes
  - RankNTypes
  - RecordWildCards
  - RecursiveDo
  - ScopedTypeVariables
  - StandaloneDeriving
  - StrictData
  - TemplateHaskell
  - TupleSections
  - TypeApplications
  - TypeFamilies
  - TypeOperators
  - UndecidableInstances
  - UndecidableSuperClasses
  - ViewPatterns

dependencies:
  - name: base
    version: ">= 4.7 && < 5"

ghc-options:
  - -Weverything
  - -Wno-missing-exported-signatures
  - -Wno-missing-import-lists
  - -Wno-missed-specialisations
  - -Wno-all-missed-specialisations
  - -Wno-unsafe
  - -Wno-safe
  - -Wno-missing-local-signatures
  - -Wno-monomorphism-restriction
  - -Wno-implicit-prelude
  - -Wno-missing-safe-haskell-mode
  - -Wno-prepositive-qualified-module
  - -Wno-missing-kind-signatures
  - -Wno-operator-whitespace
  - -Wno-unticked-promoted-constructors
  - -Wno-implicit-lift

library:
  source-dirs: src
  default-extensions: [ RebindableSyntax ]
  ghc-options: [ -Wno-unused-do-bind ]

  generated-other-modules:
    - Paths_#{useUnderscore projectName}

  dependencies:
    - indigo
    - lorentz
    - text

executables:
  #{projectName}:
    main: Main.hs
    source-dirs: app

    dependencies:
      - #{projectName}
      - containers
      - morley
      - lorentz
      - optparse-applicative
      - universum
      - with-utf8

tests:
  #{projectName}-test:
    main:        Main.hs
    source-dirs: test
    build-tools: tasty-discover:tasty-discover
    ghc-options:
      - -threaded
      - -rtsopts
      - '"-with-rtsopts=-N -A64m -AL256m"'
    dependencies:
      - #{projectName}
      - cleveland
      - lorentz
      - tasty
|]

stackYaml :: Text
stackYaml = [i|resolver: indigo-dependence-snapshot.yaml

packages:
- .

nix:
  packages: [zlib]

|]

indigoSnapshotYaml :: Text
indigoSnapshotYaml = $(embedStringFile "./snapshots/indigo-snapshot.yaml")

data DependenceSnapshotVersion
  = DSVGitCommitSha Text
  | DSVPublishedVersion Version

indigoDependenceSnapshotYaml :: DependenceSnapshotVersion -> Text
indigoDependenceSnapshotYaml dsv = [i|
\# SPDX-FileCopyrightText: #{year} Oxhead Alpha
\# SPDX-License-Identifier: LicenseRef-MIT-OA

name: indigo-dependence-snapshot

\# See more extra-deps in 'indigo-snapshot.yaml'.
resolver: indigo-snapshot.yaml

\# The 'packages' here act as 'extra-deps'.
packages:#{package}|]
  where
    package :: Text
    package = case dsv of
      DSVGitCommitSha rev -> [i|
- git: https://gitlab.com/morley-framework/indigo.git
  commit: #{rev}
|]
      DSVPublishedVersion ver -> [i|
- indigo-#{showVersion ver}
|]
    -- we're just reading system clock here, this is pretty safe;
    -- besides, we're only calling this once per run, so sharing caveats
    -- don't apply;
    year = unsafePerformIO $ formatTime defaultTimeLocale "%Y" <$> getCurrentTime
