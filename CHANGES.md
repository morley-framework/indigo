Unreleased
==========
<!-- Append new entries here -->

* [!56](https://gitlab.com/morley-framework/indigo/-/merge_requests/56)
  Update to LTS-21 (GHC 9.4.5)
  + Generalizes Indigo's integer literals to make type specifiers optional in
    cases where the type is known from immediate context.
* [!55](https://gitlab.com/morley-framework/indigo/-/merge_requests/55)
  Update morley &c

0.6.0
==========
* [!43](https://gitlab.com/morley-framework/indigo/-/merge_requests/43)
  Add emit instruction
  + New user-facing function `Indigo.Frontend.Language.emit` to emit contract
    events.
  + `annQ` and `FieldAnn` are re-exported from `Indigo.Frontend.Language` and
    consequently `Indigo`.
* [!42](https://gitlab.com/morley-framework/indigo/-/merge_requests/42)
  Updates for kathmandu
  + Bump dependencies
  + Fix typo in "division by zero" error text (was "devision by zero") in the
    expression code generator.
* [!41](https://gitlab.com/morley-framework/indigo/-/merge_requests/41)
  Updates for jakarta and GHC-9
  + Bump dependencies
  + Bump compiler/stack resolver
  + Bump docker base image
  + Note that due to Jakarta changes, some functions may need `IsNotInView`
    constraint now, exported from `Indigo.Lorentz` and `Indigo`.
* [!35](https://gitlab.com/morley-framework/indigo/-/merge_requests/35)
  Add upgrade command; generate indigo-dependency-snapshot.yaml on demand
  + Add an `indigo upgrade` command, building the latest indigo version
  + Also update project generator for latest indigo/morley
* [!36](https://gitlab.com/morley-framework/indigo/-/merge_requests/36)
  Reorganize Indigo modules
  - Get rid of `Indigo.Internal` and `Indigo.Internal.Expr` "god modules"
  - Several moves:
      - `Indigo.Internal.Lookup`, `Indigo.Internal.Expr.Compilation`, `Indigo.Internal.Expr.Decompose` to `Indigo.Backend.*`
      - `Indigo.Internal.Expr.Symbolic` to `Indigo.Frontend.Expr`
      - `Indigo.Frontend.Statement` to `Indigo.Frontend.Internal.Statement` (it's not reexported)
      - `Indigo.Common.Expr.Types` to `Indigo.Common.Expr`
  - Do not re-export backend parts from `Indigo`, specifically, `Indigo.Backend.Expr.*` and `Indigo.Backend.Lookup`
  - Rename `Indigo.Internal.*` to `Indigo.Common.*` (not really internal, since it's exported from `Indigo`)
  - Rename `Indigo.Rebinded` to `Indigo.Rebound`
  - Split type definitions from `Indigo.Compilation.Sequential` into `Indigo.Compilation.Sequential.Types`
* [!34](https://gitlab.com/morley-framework/indigo/-/merge_requests/34)
  Replace `dip dip` with `dip 2` in the code generator
* [!24](https://gitlab.com/morley-framework/indigo/-/merge_requests/24)
  Bump Morley/Lorentz and comply with the changes in them.
* [!15](https://gitlab.com/morley-framework/indigo/-/merge_requests/15)
  Add `failCustomNoArg` which allows failing with just a string instead of `pair string <arg>`.
* [!18](https://gitlab.com/morley-framework/indigo/-/merge_requests/18)
  Bump over Morley/Lorentz with all the edo functionality (but tickets are not yet supported).

0.5.0
==========
* [!12](https://gitlab.com/morley-framework/indigo/-/merge_requests/12)
  Refactor some general documentation-related methods, see deprecation notes for migration instructions.

0.4
==========
* [!8](https://gitlab.com/morley-framework/indigo/-/merge_requests/8)
  Comply with the recent changes in Lorentz.
  Namely, `pack`, `unpack`, `checkSignature` and hashing instructions now operate not with `ByteString`s and `Signature`s, rather with type-safe newtypes over them.
  Added `packRaw` and `unpackRaw` with the old behaviour of `pack` and `unpack`.
* [!5](https://gitlab.com/morley-framework/indigo/-/merge_requests/5)
  Replaced `indigo-trial` name with `indigo` via docker instead.
* [!630](https://gitlab.com/morley-framework/morley/-/merge_requests/630)
  Use `:` in all library instructions instead of `&`.
* [!639](https://gitlab.com/morley-framework/morley/-/merge_requests/639)
  Update the tutorial on how to install `indigo-trial` which is a wrapper around
  docker image that contains `indigo` binary and cached Haskell dependencies.
* [!596](https://gitlab.com/morley-framework/morley/-/merge_requests/596)
  Add ability to generate debug comments for Indigo statements:
  + Added several levels of verbosity
  + Added possibility to print full stack trace among with a statement call
  + `Buildable Expr` defined

0.3.1
=====
* Update `indigo` executable:
  + Make it usable inside `docker` environment.
  + Avoid usage of `mixin` in generated project since this feature is not properly handled by `stack`.
* [!629](https://gitlab.com/morley-framework/morley/-/merge_requests/629)
  Generalize functions working on `UStore` to anything satisfying classes from `Lorentz.StoreClass`.

0.3.0
==========
* [!583](https://gitlab.com/morley-framework/morley/-/merge_requests/583)
  Add an intermediate compilation representation for optimization.
  + `fail`-like statements return `RetVars r` instead of `r`.
* [!534](https://gitlab.com/morley-framework/morley/-/merge_requests/534)
  Add a tutorial on how to setup an Indigo project using Indigo CLI.
  + Bump the dependencies version of the boilerplate generated by `indigo new`
  to the latest.

* [!566](https://gitlab.com/morley-framework/morley/-/merge_requests/566)
  Add Indigo CLI installation script.
  + Mention how Indigo CLI can be installed in the Indigo documentation.

0.2.2
=====
* [!544](https://gitlab.com/morley-framework/morley/-/merge_requests/544)
  Add `indigo` executable which improves the development of Indigo projects.
  + Create boilerplate project via `new`.
  + Wrap around stack commands: `build`, `test`, and `repl`.
  + Run `ContractRegistry` commands via `run`.

0.2.1
=====
* [!570](https://gitlab.com/morley-framework/morley/-/merge_requests/570)
  Added `coerce` and `forcedCoerce` to convert between expressions of types that
  have the same Michelson representation.
* [!558](https://gitlab.com/morley-framework/morley/-/merge_requests/558)
  Added `wrap` and `unwrap`, to generate from and extract to, values given a
  constructor with a single fields for a sum type.
* [!538](https://gitlab.com/morley-framework/morley/-/merge_requests/538)
  Add the `showcase` section presenting a list of public smart contracts written
  in Indigo.
* [!533](https://gitlab.com/morley-framework/morley/-/merge_requests/533)
  Add a tutorial on how to add documentation to a contract.
  + Create helper functions: `saveDocumentation` and `printDocumentation`
    which can generate the documentation via the REPL.
  + Add short-handed doc item statements such as: `anchor`, `description`,
    and `example`.

0.2.0
=====
* [!542](https://gitlab.com/morley-framework/morley/-/merge_requests/542)
  Use `#=` as a synonym for `//->`.

0.1.0.0
=======

Initial release.
