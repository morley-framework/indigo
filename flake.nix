# SPDX-FileCopyrightText: 2023 Oxhead Alpha
# SPDX-License-Identifier: LicenseRef-MIT-OA

{
  description = "The indigo flake";

  nixConfig.flake-registry = "https://gitlab.com/morley-framework/morley-infra/-/raw/main/flake-registry.json";

  inputs.morley-infra.url = "gitlab:morley-framework/morley-infra";

  outputs = { self, flake-utils, nixpkgs, morley-infra, ... }:
    (flake-utils.lib.eachSystem [ "x86_64-linux" ] (system:
      let
        pkgs = morley-infra.legacyPackages.${system};

        inherit (morley-infra.utils.${system}) ci-apps;

        pkgs-legacy = nixpkgs.legacyPackages.${system};

        # all local packages and their subdirectories
        # we need to know subdirectories for weeder and for cabal check
        local-packages = [
          { name = "indigo"; subdirectory = "."; }
          { name = "indigo-tutorial"; subdirectory = "./tutorial"; }
          { name = "myproject"; subdirectory = "./tutorial/standalone/myproject"; }
        ];

        # names of all local packages
        local-packages-names = map (p: p.name) local-packages;

        # source with gitignored files filtered out
        projectSrc = pkgs.haskell-nix.haskellLib.cleanGit {
          name = "indigo";
          src = ./.;
        };

        # haskell.nix package set
        hs-pkgs = pkgs.haskell-nix.stackProject {
          src = projectSrc;

          # use .cabal files for building because:
          # 1. haskell.nix fails to work for package.yaml with includes from the parent directory
          # 2. with .cabal files haskell.nix filters out source files for each component, so only the changed components will rebuild
          ignorePackageYaml = true;

          modules = [{
            packages = pkgs.lib.genAttrs local-packages-names (packageName: ci-apps.collect-hie false {
              ghcOptions = [
                # disable optimizations, fail on warnings
                "-O0" "-Werror"
              ];

              # enable haddock for local packages
              doHaddock = true;
            });

            # disable haddock for dependencies
            doHaddock = false;
          }];
        };

        # component set for all local packages like this:
        # { indigo = { library = ...; exes = {...}; tests = {...}; ... };
        #   indigo-tutorial = { ... };
        #   ...
        # }
        packages = pkgs.lib.genAttrs local-packages-names (packageName: hs-pkgs."${packageName}".components);

        flake = hs-pkgs.flake {};

      in pkgs.lib.lists.foldr pkgs.lib.recursiveUpdate {} [

        { inherit (flake) packages; }

        {
          legacyPackages = pkgs;

          devShells.default = flake.devShell;

          packages = {
            mkdocs = pkgs-legacy.callPackage ./mkdocs.nix { };

            # generate Indigo's website
            indigo-website = pkgs.runCommand "indigo-website" {
              buildInputs = [ self.packages.${system}.mkdocs ];
            } ''
              mkdir $out
              mkdir "$out/indigo-website"

              TMP_DIR=$(mktemp -d)
              cp -r ${projectSrc} "$TMP_DIR/indigo"
              chmod -R +w "$TMP_DIR/indigo"
              cd "$TMP_DIR/indigo"
              bash ./scripts/make-indigo-install.sh './tutorial/docs/install.sh'
              cd tutorial/

              mkdocs build -d "$out/indigo-website"
            '';

            all-components = pkgs.linkFarmFromDrvs "all-components"
              (builtins.attrValues flake.packages);

            default = self.packages.${system}.all-components;
          };

          checks = {
            # checks if all packages are appropriate for uploading to hackage
            run-cabal-check = morley-infra.utils.${system}.run-cabal-check {
              inherit local-packages projectSrc;
            };

            trailing-whitespace = pkgs.build.checkTrailingWhitespace ./.;

            reuse-lint = pkgs.build.reuseLint ./.;
          };

          utils.haddock = with pkgs.lib; flatten (attrValues
            (mapAttrs (pkgName: pkg: optional (pkg ? library) pkg.library.haddock) packages));
        }

        { apps = ci-apps.apps { inherit hs-pkgs local-packages projectSrc; }; }
      ]));
}
