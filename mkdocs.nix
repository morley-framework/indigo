# SPDX-FileCopyrightText: 2020 Tocqueville Group
#
# SPDX-License-Identifier: LicenseRef-MIT-TQ

# Updated version of mkdocs (and the python packages to build it), that also
# includes pymdown-extensions as well as our custom markdown extensions.

{ stdenv, lib, python38, python38Packages, fetchFromGitHub }:

let
  lunr = python38Packages.buildPythonPackage rec {
    pname = "lunr";
    version = "0.5.8";

    src = python38Packages.fetchPypi {
      inherit pname version;
      sha256 = "c4fb063b98eff775dd638b3df380008ae85e6cb1d1a24d1cd81a10ef6391c26e";
    };

    propagatedBuildInputs = with python38Packages; [ future six ];

    doCheck = false;
  };

  pymdown-extensions = python38Packages.buildPythonPackage rec {
    pname = "pymdown-extensions";
    version = "7.1";

    src = python38Packages.fetchPypi {
      inherit pname version;
      sha256 = "5bf93d1ccd8281948cd7c559eb363e59b179b5373478e8a7195cf4b78e3c11b6";
    };

    propagatedBuildInputs = [ python38Packages.markdown ];

    doCheck = false;
  };

  include-code = python38Packages.buildPythonPackage rec {
    pname = "include_code";
    version = "0.1";
    src = ./tutorial/docs/markdown-ext;
    propagatedBuildInputs = with python38Packages; [ pyyaml markdown ];
  };

in python38Packages.buildPythonApplication rec {
    pname = "mkdocs";
    version = "1.1.2";

    src = python38Packages.fetchPypi {
      inherit pname version;
      sha256 = "f0b61e5402b99d7789efa032c7a74c90a20220a9c81749da06dbfbcbd52ffb39";
    };

    propagatedBuildInputs = [
      lunr

      python38Packages.click
      python38Packages.jinja2
      python38Packages.livereload
      python38Packages.pygments
      python38Packages.markdown
      python38Packages.pyyaml
      python38Packages.tornado
      python38Packages.setuptools
      python38Packages.nltk

      include-code pymdown-extensions
    ];

    doCheck = false;
  }
