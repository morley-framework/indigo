-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

module Indigo.Tutorial.Functions.Functions
  ( functionsContract
  ) where

import Indigo

data IncrementIf
  = IsZero Integer
  | HasDigitOne Natural
  deriving stock (Generic, Show)
  deriving anyclass (IsoValue)

instance ParameterHasEntrypoints IncrementIf where
  type ParameterEntrypointsDerivation IncrementIf = EpdPlain

functionsContract :: IndigoContract IncrementIf Natural
functionsContract param = defContract do
  result <- case_ param $
    ( #cIsZero #= checkZero
    , #cHasDigitOne #= checkHasDigitOne
    )
  updateStorage result

checkZero :: Var Integer -> IndigoFunction Bool
checkZero val = defFunction do
  return (val == 0 int)

checkHasDigitOne :: Var Natural -> IndigoFunction Bool
checkHasDigitOne val = defFunction do
  base <- new$ 10 nat
  checkRes <- new$ False
  while (val > base && not checkRes) do
    val =: val / base
    remainder <- new$ val % base
    checkRes =: remainder == 1 nat
  return checkRes

updateStorage :: HasStorage Natural => Var Bool -> IndigoProcedure
updateStorage result = defFunction do
  if result then storage =: 0 nat else incrementStorage

incrementStorage :: HasStorage Natural => IndigoProcedure
incrementStorage = defFunction do
  storage += 1 nat

storage :: HasStorage Natural => Var Natural
storage = storageVar
