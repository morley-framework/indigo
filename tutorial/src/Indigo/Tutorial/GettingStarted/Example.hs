-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

module Indigo.Tutorial.GettingStarted.Example
  ( exampleContract
  ) where

import Indigo

exampleContract :: IndigoContract Integer Integer
exampleContract param = defContract do
  a <- new$ 1 int
  storageVar =: param + a

{-
Resulting Michelson Contract:

parameter int;
storage int;
code { # We put an empty list of operations at the beginning and remember its position,
       # it is updated when a new operation is made.
       NIL operation;
       SWAP;

       # Then we unpair the parameter and storage.
       UNPAIR;

       # We push an integer 1 and duplicate it for later use.
       PUSH int 1;
       DUP;

       # We duplicate the parameter and bring it to the top.
       DIP 2
           { DUP };
       DIG 2;

       # We add the integer 1 with the parameter value.
       ADD;

       # We cleanup by dropping old storage, the duplicated parameter,
       # and duplicated integer 1.
       DIP 3
           { DROP };
       DUG 2;
       DROP;
       DROP;

       # This leaves us with the newly added value. Then we can swap
       # and pair this with the NIL operation to make the final stack
       # to become `[(newStorage, emittedOperations)]`. This means it will
       # update the storage with the new value, and emit no operations.
       SWAP;
       PAIR };

-}
