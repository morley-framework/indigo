-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

module Indigo.Tutorial.SideEffects.Errors
  ( errorsContract
  ) where

import Indigo

data IncrementIf
  = IsZero Integer
  | HasDigitOne Natural
  deriving stock (Generic, Show)
  deriving anyclass (IsoValue)

instance ParameterHasEntrypoints IncrementIf where
  type ParameterEntrypointsDerivation IncrementIf = EpdPlain

errorsContract :: IndigoContract IncrementIf Natural
errorsContract param = defContract do
  case_ param $
    ( #cIsZero #= checkZero
    , #cHasDigitOne #= checkHasDigitOne
    )
  incrementStorage

checkZero :: Var Integer -> IndigoProcedure
checkZero val = defFunction do
  assert [mt|unacceptable zero value|] (val == 0 int)

checkHasDigitOne :: Var Natural -> IndigoProcedure
checkHasDigitOne val = defFunction do
  base <- new$ 10 nat
  while (val > base) do
    val =: val / base
    remainder <- new$ val % base
    checkSingleDigitOne remainder
  checkSingleDigitOne val

checkSingleDigitOne :: Var Natural -> IndigoProcedure
checkSingleDigitOne digit = do
  assert [mt|unacceptable non-one digit|] (digit == 1 nat)

incrementStorage :: HasStorage Natural => IndigoProcedure
incrementStorage = defFunction do
  storage += 1 nat

storage :: HasStorage Natural => Var Natural
storage = storageVar
