-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

module Indigo.Tutorial.SideEffects.SideEffects
  ( sideEffectsContract
  , textKeeper
  ) where

import Indigo

sideEffectsContract :: IndigoContract (Maybe MText) Address
sideEffectsContract param = defContract do
  deleg <- new$ none
  mtz <- new$ zeroMutez
  notInit <- new$ [mt|not initialized|]
  addr <- createContract textKeeper deleg mtz notInit
  ifSome param (initializeContract addr) (return ())
  storage =: addr

textKeeper :: IndigoContract MText MText
textKeeper param = defContract do
  storageVar =: param

initializeContract :: (HasSideEffects, IsNotInView) => Var Address -> Var MText -> IndigoProcedure
initializeContract addr msg = defFunction do
  mtz <- new$ zeroMutez
  ifSome (contract addr)
    (\cRef -> transferTokens msg mtz cRef)
    failForRef

failForRef :: IndigoProcedure
failForRef = failUnexpected_ @() [mt|failed to create contract reference|]

storage :: HasStorage Address => Var Address
storage = storageVar
