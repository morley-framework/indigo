<!--
SPDX-FileCopyrightText: 2022 Oxhead Alpha

SPDX-License-Identifier: LicenseRef-MIT-OA
-->

# My Contract





This documentation describes an example on how to functions and procedures of Indigo.

<a name="section-Table-of-contents"></a>

## Table of contents

- [Haskell ⇄ Michelson conversion](#section-Haskell-c8644-Michelson-conversion)
- [Storage](#section-Storage)
  - [Storage](#storage-Storage)
- [Entrypoints](#section-Entrypoints)
  - [isZero](#entrypoints-isZero)
  - [hasDigitOne](#entrypoints-hasDigitOne)

**[Definitions](#definitions)**

- [Types](#section-Types)
  - [()](#types-lparenrparen)
  - [Integer](#types-Integer)
  - [Natural](#types-Natural)
  - [Text](#types-Text)
- [Errors](#section-Errors)
  - [InternalError](#errors-InternalError)
  - [IsNotZeroError](#errors-IsNotZeroError)



<a name="section-Haskell-c8644-Michelson-conversion"></a>

## Haskell ⇄ Michelson conversion

This smart contract is developed in Haskell using the [Morley framework](https://gitlab.com/morley-framework/morley). Documentation mentions Haskell types that can be used for interaction with this contract from Haskell, but for each Haskell type we also mention its Michelson representation to make interactions outside of Haskell possible.

There are multiple ways to interact with this contract:

* Use this contract in your Haskell application, thus all operation submissions should be handled separately, e.g. via calling `tezos-client`, which will communicate with the `tezos-node`. In order to be able to call `tezos-client` you'll need to be able to construct Michelson values from Haskell.

  The easiest way to do that is to serialize Haskell value using `lPackValue` function from [`Lorentz.Pack`](https://gitlab.com/morley-framework/morley/-/blob/2441e26bebd22ac4b30948e8facbb698d3b25c6d/code/lorentz/src/Lorentz/Pack.hs) module, encode resulting bytestring to hexadecimal representation using `encodeHex` function. Resulting hexadecimal encoded bytes sequence can be decoded back to Michelson value via `tezos-client unpack michelson data`.

  Reverse conversion from Michelson value to the Haskell value can be done by serializing Michelson value using `tezos-client hash data` command, resulting `Raw packed data` should be decoded from the hexadecimal representation using `decodeHex` and deserialized to the Haskell value via `lUnpackValue` function from [`Lorentz.Pack`](https://gitlab.com/morley-framework/morley/-/blob/2441e26bebd22ac4b30948e8facbb698d3b25c6d/code/lorentz/src/Lorentz/Pack.hs).

* Construct values for this contract directly on Michelson level using types provided in the documentation.

<a name="section-Storage"></a>

## Storage

<a name="storage-Storage"></a>

---

### `Storage`

Contract storage description.

**Structure:**
  * ***lastInput*** :[`Natural`](#types-Natural)
  * ***total*** :[`Natural`](#types-Natural)

**Final Michelson representation:** `pair nat nat`



<a name="section-Entrypoints"></a>

## Entrypoints

<a name="entrypoints-isZero"></a>

---

### `isZero`

Increment storage by 1 if the input is zero, otherwise fail.

**Argument:**
  + **In Haskell:** [`Integer`](#types-Integer)
  + **In Michelson:** `int`
    + **Example:** <span id="example-id">`0`</span>

<details>
  <summary><b>How to call this entrypoint</b></summary>

0. Construct an argument for the entrypoint.
1. Call contract's `isZero` entrypoint passing the constructed argument.
</details>
<p>



**Possible errors:**
* [`IsNotZeroError`](#errors-IsNotZeroError) — Fail when the input to `IsZero` entrypoint is not zero.



<a name="entrypoints-hasDigitOne"></a>

---

### `hasDigitOne`

Increment storage by 1 if the input has one digit.

**Argument:**
  + **In Haskell:** [`Natural`](#types-Natural)
  + **In Michelson:** `nat`
    + **Example:** <span id="example-id">`1`</span>

<details>
  <summary><b>How to call this entrypoint</b></summary>

0. Construct an argument for the entrypoint.
1. Call contract's `hasDigitOne` entrypoint passing the constructed argument.
</details>
<p>









# Definitions

<a name="section-Types"></a>

## Types

<a name="types-lparenrparen"></a>

---

### `()`

Unit primitive.

**Structure:** ()

**Final Michelson representation:** `unit`



<a name="types-Integer"></a>

---

### `Integer`

Signed number.

**Final Michelson representation:** `int`



<a name="types-Natural"></a>

---

### `Natural`

Unsigned number.

**Final Michelson representation:** `nat`



<a name="types-Text"></a>

---

### `Text`

Michelson string.

This has to contain only ASCII characters with codes from [32; 126] range; additionally, newline feed character is allowed.

**Final Michelson representation:** `string`



<a name="section-Errors"></a>

## Errors

Our contract implies the possibility of error scenarios, this section enlists
all values which the contract can produce via calling `FAILWITH` instruction
on them. In case of error, no changes to contract state will be applied.

Each entrypoint also contains a list of errors which can be raised during its
execution; only for no-throw entrypoints this list will be omitted.
Errors in these lists are placed in the order in which the corresponding
properties are checked unless the opposite is specified. I.e., if for a
given entrypoint call two different errors may take place, the one which
appears in the list first will be thrown.

The errors are represented either as a string `error tag` or a pair `(error tag, error argument)`.
See the list of errors below for details.

We distinquish several error classes:
+ **Action exception**: given action cannot be performed with
  regard to the current contract state.

  Examples: "insufficient balance", "wallet does not exist".

  If you are implementing a middleware, such errors should be propagated to
  the client.

+ **Bad argument**: invalid argument supplied to the entrypoint.

  Examples: entrypoint accepts a natural number from `0-3` range, and you
  supply `5`.

  If you are implementing a middleware, you should care about not letting
  such errors happen.

+ **Internal**: contract-internal error.

  In ideal case, such errors should not take place, but still, make sure
  that you are ready to handle them. They can signal either invalid contract
  deployment or a bug in contract implementation.

  If an internal error is thrown, please report it to the author of this contract.


<a name="errors-InternalError"></a>

---

### `InternalError`

**Class:** Internal

**Fires if:** Some internal error occurred.

**Representation:** Textual error message, see [`Text`](#types-Text).

<a name="errors-IsNotZeroError"></a>

---

### `IsNotZeroError`

**Class:** Action exception

**Fires if:** Fail when the input to `IsZero` entrypoint is not zero.

**Representation:** `(IsNotZeroError, <error argument>)`

Provided error argument will be of type ([`Text`](#types-Text), [`()`](#types-lparenrparen)).
