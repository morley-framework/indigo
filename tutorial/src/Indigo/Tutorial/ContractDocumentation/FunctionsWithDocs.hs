-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

{-# OPTIONS -Wno-orphans #-}

module Indigo.Tutorial.ContractDocumentation.FunctionsWithDocs
  ( myContract
  ) where

import Indigo

data Parameter
  = IsZero Integer
  | HasDigitOne Natural
  deriving stock (Generic, Show)
  deriving anyclass (IsoValue)

[entrypointDoc| Parameter plain |]

data Storage = Storage
  { sLastInput :: Natural
  , sTotal :: Natural
  }
  deriving stock (Generic, Show)
  deriving anyclass (IsoValue, HasAnnotation)

[typeDoc| Storage "Contract storage description."|]

myContract :: IndigoContract Parameter Storage
myContract param = defContract $ docGroup "My Contract" do
  contractGeneralDefault
  description
    "This documentation describes an example on how to functions and \
    \procedures of Indigo."
  doc $ dStorage @Storage
  entryCaseSimple param
    ( #cIsZero #= checkZero
    , #cHasDigitOne #= checkHasDigitOne
    )

checkZero
  :: forall tp.
     ( tp :~> Integer
     , HasStorage Storage
     )
  => IndigoEntrypoint tp
checkZero val = do
  description "Increment storage by 1 if the input is zero, otherwise fail."
  example (0 int)
  result <- new$ (val == 0 int)
  if result then
    incrementStorage
  else
    failCustom_ @() #isNotZeroError

checkHasDigitOne
  :: forall tp.
     ( tp :~> Natural
     , HasStorage Storage
     )
  => IndigoEntrypoint tp
checkHasDigitOne val = do
  description "Increment storage by 1 if the input has one digit."
  example (1 int)
  currentVal <- new$ val
  setStorageField @Storage #sLastInput currentVal
  base <- new$ 10 nat
  checkRes <- new$ False
  while (val > base && not checkRes) do
    currentVal =: currentVal / base
    remainder <- new$ val % base
    checkRes =: remainder == 1 nat
  updateStorage checkRes

updateStorage :: HasStorage Storage => Var Bool -> IndigoProcedure
updateStorage result = defFunction do
  if result then
    setStorageField @Storage #sTotal $ 0 nat
  else
    incrementStorage

incrementStorage :: HasStorage Storage => IndigoProcedure
incrementStorage = defFunction do
  currentTotal <- getStorageField  @Storage #sTotal
  setStorageField @Storage #sTotal (currentTotal + (1 nat))

-- | Custom error which can be used via: @failCustom_ #myError@
[errorDocArg| "isNotZeroError" exception "Fail when the input to `IsZero` entrypoint is not zero." ()|]
