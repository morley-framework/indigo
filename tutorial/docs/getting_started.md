<!--
SPDX-FileCopyrightText: 2020 Tocqueville Group

SPDX-License-Identifier: LicenseRef-MIT-TQ
-->

# Getting started

This tutorial will guide you in developing Michelson contracts using Indigo.
It will present the features of the language and provide some examples as well.

## Requirements

This assumes you have some knowledge about Tezos and Michelson, if you don't you
can learn about them by reading [tezos' documentation](http://tezos.gitlab.io/)
and following our [Michelson tutorial](https://gitlab.com/morley-framework/michelson-tutorial).

On the software side you will need to install [Indigo CLI](index.md#installing-indigo-cli)
for compilation and grab the tutorial source code from:

```
git clone https://gitlab.com/morley-framework/indigo.git
cd indigo/tutorial
```

!!! tip "Tutorial resources"
    All the resources for this tutorial, including this documentation and the
    examples' source code, are located in the [`tutorial/`](https://gitlab.com/morley-framework/indigo/-/tree/master/tutorial)
    subfolder.

## Setup recommendations and first steps

If you are not a Haskell developer and have no intention of becoming one, you
should still be able to follow the tutorial with its examples and write your own
contracts as well.

We can start by opening up an interactive shell with Indigo loaded in it.

!!! tip "Cleaner REPL prompt"
    Before opening the shell you are advised to modify the prompt, as the
    standard one contains a lot of text. To use a simple one you can simply copy
    the `.ghci` file to your home directory:

    {!bash 5-* .ghci!}

Provided that you have installed [Indigo CLI](index.md#installing-indigo-cli), you can open a REPL by issuing, from
the root directory of the cloned repository:
```bash
indigo repl
```

!!! note ""
    It will take some time to compile everything the first time around, but it
    will get faster later on, I promise.

You can now load a source file by using the `:load` command, we can start by
loading the example code provided in this chapter:

{!haskell 5-14 src/Indigo/Tutorial/GettingStarted/Example.hs!}

```bash
:load src/Indigo/Tutorial/GettingStarted/Example.hs
```

!!! warning ""
    For these examples, you may receive warnings about `missing-home-modules`,
    you can safely ignore these.

Now you have access to the contracts exported by this module (the ones on the
list at the top of the file), that in this case is just `exampleContract`.
We'll go through what is the syntax of Indigo, but if you take a quick look at
`exampleContract` you should be able to see that it implements a simple contract
that takes an `Integer` parameter, creates a new variable `a` of value `1`, and
finally sets its storage with the sum of `param` and `a`. Very simple.

You can compile it and see what's the Michelson code for this (or any other)
Indigo contract by using:
```haskell
printAsMichelson @Integer @Integer exampleContract
```
or save it to a file with:
```haskell
saveAsMichelson @Integer @Integer exampleContract "<path-to-file>.tz"
```
that you can run and deploy on the network!

<!--
TODO try to remove the need for this.
-->
Note that it is necessary for both to specify the parameter and storage types
(in this order), that in this case are just `@Integer`.

With these tools at your disposal you will be able to create Indigo contracts and
compile them to Michelson, just by using the REPL.

If you are ready for it, you should head straight for the next chapter:
[Basics and Variables](basics.md).

## Technical details: developing using Indigo

If you are a Haskell developer interested in developing your own library/contract
using Indigo, you'll probably be better off following this tutorial (that will
contain more of these technical paragraphs) and checking the Haddock documentation
as well.

!!! tip "Generate Haddock documentation"
    You can generate Haddock documentation for indigo from
    [its folder](https://gitlab.com/morley-framework/indigo/-/tree/master/)
    by using:

        :::bash
        make haddock

To be able to write your own project you'll probably just need to
```haskell
import Indigo
```
in your modules and pay attention to enabling the right language extensions.

You may not need all of them, but some are necessary and allow the language to
be neatly usable, for example `RebindableSyntax` is what makes it possible to use
the do-notation and Haskell's standard `if ... then ... else` statements (more
details on this will come later).
