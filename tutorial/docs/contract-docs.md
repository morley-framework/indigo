<!--
SPDX-FileCopyrightText: 2020 Tocqueville Group

SPDX-License-Identifier: LicenseRef-MIT-TQ
-->

# Add documentation to the contract

In this chapter, we will show how to add documentation to our smart contract
directly in the code.

We will use a contract that resembles the one in section [Functions and Procedures](functions.md).

You can load the file below into the REPL to follow along with the tutorial.

## Contract Name and Description

First let's get started with something simple, we can specify the contract name and description by
using `docGroup` and `description` like below:

{!haskell 30-35 src/Indigo/Tutorial/ContractDocumentation/FunctionsWithDocs.hs!}

You can generate the documentation of this contract by using:

```haskell
saveDocumentation @Parameter @Storage myContract "my-documentation.md"
```

!!! note "Note"
    You can also see the documentation immediately in the REPL using:

    ```
    printDocumentation @Parameter @Storage myContract
    ```

If there is no error, you will be able to see a file named `my-documentation.md` .

We can see that, at the top of the documentation, there are the name and description of our contract.
There is also Table of Contents which is generated automatically for us.

{!markdown 7-18 src/Indigo/Tutorial/ContractDocumentation/documentation.md!}

Beside `docGroup` and `description`, there are also other doc items that we could use:

- `anchor <link>`: Create custom anchor in the generated documentation.
- `example <value>`: Use inside an entrypoint to specify an example value for that entrypoint.

There are also many other things that are generated automatically for us, including:
`Entrypoints` and `Definitions`.



## Entrypoints' documentation

We can use `description` and `example` as described above to add some information about
our entrypoints in the documentation.

{!haskell 41-55 src/Indigo/Tutorial/ContractDocumentation/FunctionsWithDocs.hs!}

{!haskell 57-73 src/Indigo/Tutorial/ContractDocumentation/FunctionsWithDocs.hs!}

Now we should have description that looks like this:

{!markdown 68-70 src/Indigo/Tutorial/ContractDocumentation/documentation.md!}
{!markdown 96-98 src/Indigo/Tutorial/ContractDocumentation/documentation.md!}

!!! note "Note"
    There are a lot of other useful things that are generated as well, such as:
    <br>   - __How to call the entrypoint__: Instruction on how to call the entrypoint
    <br>   - __Type__: Haskell and Michelson Types.
    <br>   - __Example__: Example value for Michelson
    <br>&nbsp;&nbsp;- We can modify this value with `example` as described above

## Custom error messages

When handling an entrypoint's input, there are times when the user gives an unexpected value.
In this case, we would like to fail with a nice error message.

This is where custom error messages come into play.

We can define custom error messages like below:
{!haskell 89-89 src/Indigo/Tutorial/ContractDocumentation/FunctionsWithDocs.hs!}

As you can see, there are 3 parts to defining a custom error message:

- `"isNotZeroError"` is the name of the error.
- `exception` is the type of the error.  There are 4 types of error
you can specify:
    - `exception`: Normal expected error. Examples: "insufficient balance", "wallet does not exist".
    - `bad-argument`: Invalid argument passed to entrypoint. Examples: your entrypoint accepts an enum represented as `nat`, and unknown value is provided.
    - `contract-internal`: Unexpected error. Most likely it means that there is a bug in the contract or the contract has been deployed incorrectly.
    - `unknown`: When the error type is beside the other 3.
- ``"Fail when the input to `IsZero` entrypoint is not zero."`` is the description of the error.

This is how you would use your custom error message.

{!haskell 55-55 src/Indigo/Tutorial/ContractDocumentation/FunctionsWithDocs.hs!}

The error message entry will be generated like below:

{!markdown 236-243 src/Indigo/Tutorial/ContractDocumentation/documentation.md!}

## Generate documentations for storage

Let's say that we have a storage type like this:

{!haskell 21-26 src/Indigo/Tutorial/ContractDocumentation/FunctionsWithDocs.hs!}


We may want to document this type in our documentation. Here's how we can do it:

{!haskell 28-28 src/Indigo/Tutorial/ContractDocumentation/FunctionsWithDocs.hs!}

We also need to add `docStorage` in the contract code as well:
{!haskell 30-39 src/Indigo/Tutorial/ContractDocumentation/FunctionsWithDocs.hs!}

This will generate the doc entry like below:

{!markdown 50-58 src/Indigo/Tutorial/ContractDocumentation/documentation.md!}

Your final code should look like this:
{!haskell 6-* src/Indigo/Tutorial/ContractDocumentation/FunctionsWithDocs.hs!}

We can now proceed to the final chapter that explains how to setup a proper
Indigo project, which will improve your development with `indigo`, and also give
an overview on the usage of the Indigo CLI:
[Setting up a project](project-setup.md).
