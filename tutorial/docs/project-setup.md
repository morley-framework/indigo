<!--
SPDX-FileCopyrightText: 2020 Tocqueville Group

SPDX-License-Identifier: LicenseRef-MIT-TQ
-->

# Setting up a project

In the previous chapters, we saw how to interact with the contract
code that we wrote via the REPL.

Now, it's time to setup a proper Indigo project, which will allow us to split
files in an organized way, add multiple contracts and give us many other useful
commands to interact with them.

!!! note "Note"
    Make sure that you have installed the [Indigo CLI](index.md#installing-indigo-cli)
    and gone through all the previous chapters proceeding into this.

## Directory Structure

You can set up a new project, here called `myproject`, using:
```bash
indigo new myproject # Creating a boilerplate project
cd myproject
indigo build # Build the project
indigo run list # Run the command `list`
```

If everything goes well, you will see this in the output:
```
Available contracts: - Basic
```

We are going to use our previous chapter's contract as an example on how to
organize the files structure.

{!haskell 5-* src/Indigo/Tutorial/ContractDocumentation/FunctionsWithDocs.hs!}

We are going to split this file into 3 parts: `Doc.hs`, `Types.hs`, and `Impl.hs`
like below:

{!haskell 5-* standalone/myproject/src/MyContract/Doc.hs!}

{!haskell 5-* standalone/myproject/src/MyContract/Types.hs!}

{!haskell 5-* standalone/myproject/src/MyContract/Impl.hs!}

Then we have another module that imports these all together.

{!haskell 5-* standalone/myproject/src/MyContract.hs!}

!!! note "Note"
    Up to this point we have used the export list to make contracts available to
    the REPL and `import` to made `Indigo` functions and types available to a module.
    In the same way, we can also `export` contracts/types/functions from a module and use
    `import` in another one to make them available in the latter.

### Setting up CLI

Next, we are going to import our contract in our `Main.hs` so that we can interact
with the contract.

Do not worry about understanding everything, most of the things are boilerate and generated
by Indigo CLI.

{!haskell 5-* standalone/myproject/app/Main.hs!}

Pay attention to this part, this is how you add multiple contracts to the project.
{!haskell 36-50 standalone/myproject/app/Main.hs!}

To include our contract in the CLI, you have to specify these 4 fields:

- `ciContract`: Simply put our contract here
- `ciIsDocumented`: Set to `True` to allow generating documentation for this contract
- `ciStorageParser`: Set to `Just (pure MyContract.emptyStorage)` to add the <br>
  commands `storage-MyContract` for printing the initial storage of our contract.
- `ciStorageNotes`: is for customizing the generated annotations of our contract.
  Since we do not want to change any annotations for now, we can simply set to `Nothing`.

With this setup, running `indigo run list` will output

```
Available contracts:
  - Basic
  - MyContract
```

## Testing

We have a simple test for our contract:
{!haskell 5-* standalone/myproject/test/Test/MyContract.hs!}

We will go into the details of writing the tests for `indigo` later on,
but basically in this test, we call the entrypoint `HasDigitOne` with the value 1.

And we expect the updated storage to be:
```
Storage
  { sLastInput = 1
  , sTotal = 1
  }
```

To run the test, simply run `indigo test`.

!!! warning "Note"
    If your `indigo` CLI is installed via docker, this command is not
    supported yet.

## Indigo CLI Usage

The advantage of setting up the project is that it allows us to do more things
compared to using the REPL.

As you can see above, we have introduced a few new commands and now it is time to
go into details.

The main commands of Indigo CLI are:

- `indigo new <project-name>`: Generate a starter indigo project.
- `indigo build`: Build the current project.
- `indigo run <command>`: Run a command in the current project.
- `indigo test`: Run all the tests in the project.
- `indigo repl`: Open the Indigo REPL.
- `indigo upgrade`: Build and install the latest `indigo` version.

The main way that we are going to interact with our contract are through
`indigo run` commands.

To see what are the available run commands, we can execute `indigo run --help`.

The arguments to the `indigo run` commands are:

- `list`: This is the commands that we already encounter. It basically prints out
all the contracts we have in our project.

- `print`: Print a contract in its Michelson form.
    - Example: `indigo run print -n MyContract`
    - Result: This will output the michelson code in `MyContract.tz`

- `document`: Print the documentation of a contract. This is similar to the REPL function:
`saveDocumentation`.
    - Example: `indigo run document -n MyContract`
    - Result: This will output the documentation in `MyContract.md`

- `analyze`: Analyze the contract and prints statistic about it.
    - Example: `indigo run analyze -n MyContract`

- `storage-<contract-name>`: Print initial storage for the given contract.
    - Example: `indigo run storage-MyContract`
    - Result: This simply outputs `Pair 0 0`.

This concludes our tutorial, we hope you can use the informations that you now possess to develop new and exciting Michelson contracts with Indigo.
