<!--
SPDX-FileCopyrightText: 2020 Tocqueville Group

SPDX-License-Identifier: LicenseRef-MIT-TQ
-->

# Showcase

The following is a list of some of the public smart contracts using Indigo.

- [Morley Ledgers](https://gitlab.com/morley-framework/morley-ledgers): This package contains Indigo contracts implementing common Ledger interfaces:
    - `AbstractLedger` contract provides a trivial implementation of the AbstractLedger interface.
    - `ManagedLedger` contract provides a sample implementation of the ApprovableLedger interface.
- [Globacap](https://github.com/serokell/tezos-globacap/): Tezos Globacap project provides two entities:
    - `Safelist` contract which is used to restrict and regulate share transfers.
    - `Holdings` contract which is used to distribute the token and optionally regulated by the safelist contract.

!!! tip "Contribution"
    You can submit your smart contract to be a part of this list by using the `Edit on GitLab` on the top
    of this page or by creating a <!-- xrefcheck: ignore link --> [new merge request](https://gitlab.com/morley-framework/indigo/-/merge_requests/new).
