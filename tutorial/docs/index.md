<!--
SPDX-FileCopyrightText: 2020 Tocqueville Group

SPDX-License-Identifier: LicenseRef-MIT-TQ
-->

!!! warning "deprecation"
    **Note: this project is deprecated.**

    It is no longer maintained since the activation of protocol "Nairobi" on the Tezos mainnet (June 24th, 2023).

# Indigo eDSL

Indigo eDSL is a high level language for Michelson contract development.

It is meant first and foremost to free you from the burden of manual stack
management and supports common features of imperative languages.

Even with a very small example the difference in ease of use is quite visible:


=== "Indigo"

    {!haskell 11-14 src/Indigo/Tutorial/GettingStarted/Example.hs!}

=== "Michelson"

    {!bash 19-38 src/Indigo/Tutorial/GettingStarted/Example.hs!}

You can find larger examples [here](showcase.md).

## Overall idea

Michelson contracts are stack-based and often follow the repetitive pattern of
copying the required values on top of the stack to apply an instruction to them.
Indigo can associate variables to values in the stack and it's able to refer to
them in order to handle this pattern automatically.

In addition to this it can override existing variables with new values, allowing
the manipulation of the stack to be automatic.

Leveraging this ability, it also supports features such as: imperative statements
(`if`, `while`, ...), expressions, operators (`+`, `||`, `==`, ...), scoped
functions definition and errors.

Indigo is built on top of `Lorentz`, which in turn is built on top of `Morley`, a
dialect that is a superset of vanilla Michelson.
If you are interested more in the relationships between these projects you can
head over to [the repo on GitLab](https://gitlab.com/morley-framework/morley).

Indigo uses Morley to map values from Michelson to Haskell and to compile to
Michelson code (or to any of the other projects in the chain mentioned above).

## Installing Indigo CLI

### Installing Indigo CLI via docker

Indigo CLI provides commands for development and interaction with Indigo project.

For new users who want faster installation just to try `indigo` out, we recommend
installing `indigo` via docker.

For POSIX-compliant operating systems, simply run:

```sh
curl https://gitlab.com/morley-framework/indigo/-/raw/production/scripts/indigo.sh > ~/.local/bin/indigo
chmod +x ~/.local/bin/indigo
indigo --help
```

This is a wrapper over docker image that contains indigo binary and cached Haskell
dependencies which provides faster insllation time for the sake of slightly slower performance
and the inability to run `test` command.

### Installing Indigo CLI from source

Before installing Indigo CLI by building from source, it's recommended to update the package
lists of you OS repository before installing indigo via installation script.
E.g. you should run `sudo apt-get update` if you're using Ubuntu.

For POSIX-compliant operating systems, the easiest way to install is to run:

```sh
curl -sSL https://indigo-lang.gitlab.io/install.sh | bash
```

or:

```sh
wget -qO- https://indigo-lang.gitlab.io/install.sh | bash
```

Note that installing Indigo CLI from source and using it to build a project for the
very first time can take quite a bit of time. Subsequent usage will be much faster.

Also note that the `https://indigo-lang.gitlab.io/install.sh` script may ask for
root access using `sudo` in order to use your platform's package manager to install
dependencies and to install to `/usr/local/bin`.

The script also expects `~/.local/bin` to be in $PATH to be able to install Indigo
CLI properly.

## Learning and using Indigo

An introduction to the syntax and the features of the Indigo language, as well as
instructions on how to use it, are available in the form of a step-by-step tutorial.

This aims to be accessible to as many people as possible, but also contains more
technical informations for Haskell developers that are interested in learning more.

For this reason anyone interested in learning more about the language in practice
should start by taking a look at its [Getting started page](getting_started.md).

## Additional documentation

In addition to the tutorial, this documentation contains full reference pages
and, as for the other Morley projects, Indigo has [Haddock documentation](https://hackage.haskell.org/package/indigo).

## Contributing to this website

If you want to report a bug or request changes to this website you can do so by
creating a <!-- xrefcheck: ignore link --> [new GitLab issue](https://gitlab.com/morley-framework/indigo/-/issues/new?issue[title]=Indigo%20website:%20).

If you instead would like to propose changes, you can use the `Edit on GitLab`
links at the top of every page, or open a <!-- xrefcheck: ignore link --> [new merge request](https://gitlab.com/morley-framework/indigo/-/merge_requests/new).

The source for this website can be found [here in the repository](https://gitlab.com/morley-framework/indigo/-/tree/master/tutorial)
where the README contains info on how it's built.
