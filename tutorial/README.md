<!--
SPDX-FileCopyrightText: 2020 Tocqueville Group

SPDX-License-Identifier: LicenseRef-MIT-TQ
-->

> :warning: **Note: this project is deprecated.**
>
> It is no longer maintained since the activation of protocol "Nairobi" on the Tezos mainnet (June 24th, 2023).

# Indigo Tutorial

This is a tutorial for [Indigo eDSL](https://gitlab.com/morley-framework/indigo/-/tree/master/).

It is built using [mkdocs](https://mkdocs.readthedocs.io/en/stable/) and the
following extensions:
- [pymdown-extensions](https://pypi.org/project/pymdown-extensions)
- [include_code](https://gitlab.com/morley-framework/indigo/-/tree/master/tutorial/docs/markdown-ext)

## Building locally

You can build the website locally by installing the tool and packages mentioned
above, see their documentation for further instructions.

You can then use `mkdocs serve` or `mkdocs build` to check the resulting website.

Alternatively, you can build the website using [`nix`](https://nixos.org/),
by issuing, from the root of this repository:
```bash
nix-build ci.nix -A indigo-website
```

which will produce the resulting static files in the `/result/indigo-website`
directory.

## Results for a Merge Request

If you are trying to push your changes to the website and opened a Merge Request,
you can check the resulting files in the pipeline's job artifacts.
