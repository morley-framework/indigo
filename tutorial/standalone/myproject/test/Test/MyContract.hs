-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

-- deriveRPC creates an orphan HasRPCRepr instance
{-# OPTIONS_GHC -Wno-orphans #-}

module Test.MyContract
  ( test_MyContract_updates_storage_properly
  ) where

import MyContract (Parameter(..), Storage(..), emptyStorage, myContractLorentz)

import Lorentz
import Test.Cleveland
import Test.Tasty (TestTree)

deriveRPC "Storage"

test_MyContract_updates_storage_properly :: TestTree
test_MyContract_updates_storage_properly = testScenario "mycontract test" $ scenario do
  contractAddr <- originate "my-contract" emptyStorage $ mkContract myContractLorentz

  transfer contractAddr $ calling def (HasDigitOne 1)

  storage <- getStorage contractAddr
  sLastInputRPC storage @== 1
  sTotalRPC storage @== 1
