-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

module Test.Basic
  ( test_Basic_updates_storage_properly
  ) where

import Basic (basicContractLorentz)

import Lorentz (mkContract)
import Test.Cleveland
import Test.Tasty (TestTree)

test_Basic_updates_storage_properly :: TestTree
test_Basic_updates_storage_properly = testScenario "Basic test" $ scenario do
  contractAddr <- originate "basic" (10 :: Integer) $ mkContract basicContractLorentz

  transfer contractAddr $ calling def 90

  getStorage contractAddr @@== 100
