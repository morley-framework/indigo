-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

module MyContract.Impl
  ( myContractLorentz
  , emptyStorage
  ) where

import Indigo

import MyContract.Doc
import MyContract.Types

myContractLorentz :: ContractCode Parameter Storage
myContractLorentz = compileIndigoContract myContract

emptyStorage :: Storage
emptyStorage = Storage (0 nat) (0 nat)

myContract :: IndigoContract Parameter Storage
myContract param = defContract $ docGroup "My Contract" do
  contractGeneralDefault
  description contractDoc
  doc $ dStorage @Storage
  entryCaseSimple param
    ( #cIsZero #= checkZero
    , #cHasDigitOne #= checkHasDigitOne
    )

checkZero
  :: forall tp.
     ( tp :~> Integer
     , HasStorage Storage
     )
  => IndigoEntrypoint tp
checkZero val = do
  description checkZeroDoc
  example (0 int)
  result <- new$ (val == 0 int)
  if result then
    incrementStorage
  else
    failCustom_ @() #isNotZeroError

checkHasDigitOne
  :: forall tp.
     ( tp :~> Natural
     , HasStorage Storage
     )
  => IndigoEntrypoint tp
checkHasDigitOne val = do
  description checkHasDigitOneDoc
  example (1 int)
  currentVal <- new$ val
  setStorageField @Storage #sLastInput currentVal
  base <- new$ 10 nat
  checkRes <- new$ False
  while (val > base && not checkRes) do
    currentVal =: currentVal / base
    remainder <- new$ val % base
    checkRes =: remainder == 1 nat
  updateStorage checkRes

updateStorage :: HasStorage Storage => Var Bool -> IndigoProcedure
updateStorage result = defFunction do
  if result then
    setStorageField @Storage #sTotal $ 0 nat
  else
    incrementStorage

incrementStorage :: HasStorage Storage => IndigoProcedure
incrementStorage = defFunction do
  currentTotal <- getStorageField  @Storage #sTotal
  setStorageField @Storage #sTotal (currentTotal + (1 nat))
