-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

{-# OPTIONS -Wno-orphans #-}

module MyContract.Types
  ( Parameter(..)
  , Storage(..)
  ) where

import Indigo

data Parameter
  = IsZero Integer
  | HasDigitOne Natural
  deriving stock (Generic, Show)
  deriving anyclass (IsoValue)

[entrypointDoc| Parameter plain |]

data Storage = Storage
  { sLastInput :: Natural
  , sTotal :: Natural
  }
  deriving stock (Generic, Show)
  deriving anyclass (IsoValue, HasAnnotation)

[typeDoc| Storage "Contract storage description."|]

-- | Custom error which can be used via: @failCustom_ #myError@
[errorDocArg| "isNotZeroError" exception "Fail when the input to `IsZero` entrypoint is not zero." ()|]
