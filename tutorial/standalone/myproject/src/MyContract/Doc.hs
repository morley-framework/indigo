-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

module MyContract.Doc
  ( contractDoc
  , checkZeroDoc
  , checkHasDigitOneDoc
  ) where

import Indigo
import Morley.Util.Markdown (md)

contractDoc :: Markdown
contractDoc = [md|
  This documentation describes an example on how to functions and
  procedures of Indigo.
  |]

checkZeroDoc :: Markdown
checkZeroDoc =
  "Increment storage by 1 if the input is zero, otherwise fail."

checkHasDigitOneDoc :: Markdown
checkHasDigitOneDoc =
  "Increment storage by 1 if the input has one digit."
