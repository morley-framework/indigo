-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

module Basic
  ( basicContractLorentz
  , emptyStorage
  ) where

import Indigo

basicContractLorentz :: ContractCode Integer Integer
basicContractLorentz = compileIndigoContract basicContract

basicContract :: IndigoContract Integer Integer
basicContract param = defContract do
  storage += param

emptyStorage :: Integer
emptyStorage = 0 int

storage :: HasStorage Integer => Var Integer
storage = storageVar
