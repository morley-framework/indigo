-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

module MyContract
    ( Parameter(..)
    , Storage(..)
    , myContractLorentz
    , emptyStorage
    ) where

import MyContract.Impl
import MyContract.Types
