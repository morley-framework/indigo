-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

module Main
  ( main
  ) where

import Universum

import Data.Map qualified as Map
import Lorentz (DGitRevision(..), mkContract)
import Lorentz.ContractRegistry
import Main.Utf8 (withUtf8)
import Options.Applicative qualified as Opt
import Options.Applicative.Help.Pretty (Doc, linebreak)
import System.Environment (withProgName)

import Basic qualified
import MyContract qualified

programInfo :: DGitRevision -> Opt.ParserInfo CmdLnArgs
programInfo gitRev = Opt.info (Opt.helper <*> argParser contracts gitRev) $
  mconcat
  [ Opt.fullDesc
  , Opt.progDesc "Indigo CLI provides commands for development and interaction with Indigo project."
  , Opt.header "Indigo CLI"
  , Opt.footerDoc $ Just usageDoc
  ]

usageDoc :: Doc
usageDoc = mconcat
   [ "You can use help for specific COMMAND", linebreak
   , "EXAMPLE:", linebreak
   , "  indigo run print --help", linebreak
   ]

contracts :: ContractRegistry
contracts = ContractRegistry $ Map.fromList
  [ "Basic" ?:: ContractInfo
    { ciContract = mkContract $ Basic.basicContractLorentz
    , ciIsDocumented = True
    , ciStorageParser = Just (pure Basic.emptyStorage)
    , ciStorageNotes = Nothing
    }
  , "MyContract" ?:: ContractInfo
    { ciContract = mkContract $ MyContract.myContractLorentz
    , ciIsDocumented = True
    , ciStorageParser = Just (pure MyContract.emptyStorage)
    , ciStorageNotes = Nothing
    }
  ]

main :: IO ()
main = withUtf8 $ withProgName "indigo run" $ do
  cmdLnArgs <- Opt.execParser (programInfo DGitRevisionUnknown)
  runContractRegistry contracts cmdLnArgs `catchAny` (die . displayException)
