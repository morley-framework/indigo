#!/usr/bin/env bats

# SPDX-FileCopyrightText: 2020 Tocqueville Group
#
# SPDX-License-Identifier: LicenseRef-MIT-TQ

setup () {
  indigo="./scripts/indigo.sh"
  export INDIGO_TRIAL_IMAGE="indigo"
  export INDIGO_CACHE_VOLUME="indigo-cache"
}

@test "./indigo.sh build and run" {
  $indigo new test-project --revision "$CI_COMMIT_SHA"
  cd test-project
  ../$indigo build
  run ../$indigo run list
  [ "${lines[0]}" = "Available contracts: - Basic" ]
}

@test "./indigo.sh test isn't supported" {
  $indigo new test-project --revision "$CI_COMMIT_SHA"
  cd test-project
  run ../$indigo test
  [ "$status" -ne 0 ]
}

@test "./indigo.sh repl" {
  $indigo new test-project --revision "$CI_COMMIT_SHA"
  cd test-project
  echo ":q" | ../$indigo repl
}
