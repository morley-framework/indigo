#!/bin/bash -e
#
# SPDX-FileCopyrightText: 2020 Tocqueville Group
# SPDX-FileCopyrightText: 2015-2020, Stack contributors
#
# SPDX-License-Identifier: LicenseRef-MIT-TQ
# SPDX-License-Identifier: LicenseRef-MIT-Stack-contributors

set -euo pipefail
IFS=$'\n\t'

INDIGO_HOME_LOCAL_BIN="$HOME/.local/bin"

# TMP DIR for building `indigo` source
TMP_DIR=$(mktemp -d -t indigo-install-XXXXXXXXXX)

cleanup(){
    rm -rf "$TMP_DIR"
}

trap cleanup EXIT

# Check whether the given path is listed in the PATH environment variable
on_path() {
    echo ":$PATH:" | grep -q :"$1":
}

# Get path of a command
get_location() {
    command -v "$1"
}

# Check whether the given command exists
has_cmd() {
    get_location "$1" > /dev/null 2>&1
}

# Print a message to stderr and exit with error code
die() {
    # Use `printf` since `echo -e` is not portable.
    printf '%b\n' "$@" >&2
    cleanup
    exit 1
}

indigo_check_home_local_bin_on_path() {
    if on_path "$INDIGO_HOME_LOCAL_BIN" ; then
        IS_HOME_LOCAL_BIN_ON_PATH=true
    fi
}

display_path_warning(){
    if [[ -z ${IS_HOME_LOCAL_BIN_ON_PATH:-} ]] ; then
        echo "WARNING: '$INDIGO_HOME_LOCAL_BIN' is not on your PATH."
        echo "    To make sure everything works properly, we strongly"
        echo "    recommend adding it to the beginning of PATH in your profile."
        echo ""
    fi
}

check_indigo_installed() {
    if has_cmd indigo ; then
        die "$(installed_indigo_version) already appears to be installed at: \n$(get_location indigo)\n"
    fi
}

check_stack_installed() {
    if has_cmd stack ; then
        echo "Stack has already been installed."
    else
        install_stack
        stack --version
    fi
}

# Get installed Stack version, if any
installed_indigo_version() {
    indigo --version | grep -o 'indigo-\([[:digit:]]\|\.\)\+' | tr '[:upper:]' '[:lower:]'
}

install_indigo() {
    # Set temporary path needed by `stack` to install executable properly,
    # in case the user haven't add the PATH yet.
    PATH="$PATH:$INDIGO_HOME_LOCAL_BIN"

    cd "$TMP_DIR"

    # Have to use custom resolver due to 'extra-deps'
    # '****<snapshot-name>' will be replaced with indigo-snapshot.yaml via 'make-indigo-install.sh'
    cat << EOF > ./indigo-snapshot.yaml
****indigo

EOF

    cat << EOF > ./indigo-dependence-snapshot.yaml
# SPDX-FileCopyrightText: $(date +%Y) Oxhead Alpha
# SPDX-License-Identifier: LicenseRef-MIT-OA

name: indigo-dependence-snapshot

# See more extra-deps in 'indigo-snapshot.yaml'.
resolver: indigo-snapshot.yaml

# The 'packages' here act as 'extra-deps'.
packages:
****dependence

EOF

    # workaround for https://github.com/commercialhaskell/stack/issues/5258
    stack --resolver ./indigo-dependence-snapshot.yaml exec -- ghc-pkg --no-user-package-db unregister indigo || true
    stack install indigo --resolver ./indigo-dependence-snapshot.yaml --local-bin-path "$INDIGO_HOME_LOCAL_BIN"

    echo "Indigo CLI installation has been completed."
}

install_stack(){
    curl -sSL https://get.haskellstack.org/ | sh
}

# -------------------------------------------------------
# Main
# -------------------------------------------------------

# Store info whether or not PATH is set
# This has to be checked beforehand since we temporarily modify `PATH`
# during `indigo` setup.
indigo_check_home_local_bin_on_path

echo "--------------------------------------------------"
echo "Installing Stack ..."
echo "--------------------------------------------------"

check_stack_installed

echo "--------------------------------------------------"
echo "Installing Indigo CLI ..."
echo "--------------------------------------------------"

check_indigo_installed

install_indigo

display_path_warning

echo ""

trap cleanup EXIT
