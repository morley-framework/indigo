#!/usr/bin/env bash

# SPDX-FileCopyrightText: 2020 Tocqueville Group
#
# SPDX-License-Identifier: LicenseRef-MIT-TQ

# This script ensure that bad command exits with proper error code
# Note that this binary is expected to be in PATH.

set -uo pipefail

indigo new test-project --revision "$CI_COMMIT_SHA"
cd test-project || exit
indigo run badCommand

exitCode=$?
if [ $exitCode -ne 0 ]; then
    echo "The command exits with an error code as expected."
else
    echo "Error: the command unexpected passed."
    exit 1
fi
