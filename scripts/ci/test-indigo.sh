#!/usr/bin/env bash

# SPDX-FileCopyrightText: 2020 Tocqueville Group
#
# SPDX-License-Identifier: LicenseRef-MIT-TQ

# This script runs simple scenario for 'indigo' binary.
# Note that this binary is expected to be in PATH.

set -euo pipefail

indigo upgrade --revision "$CI_COMMIT_SHA" | grep 'already up to date'
indigo upgrade --revision "$CI_COMMIT_SHA" --force
indigo new test-project --revision "$CI_COMMIT_SHA"
cd test-project
# Hack to avoid `ambiguous Prelude` error.
# See https://github.com/commercialhaskell/stack/issues/5414
stack test
indigo build
indigo run print -n Basic
indigo run print
indigo test
