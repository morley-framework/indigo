#! /usr/bin/env bash

# SPDX-FileCopyrightText: 2020 Tocqueville Group
#
# SPDX-License-Identifier: LicenseRef-MIT-TQ

set -euo pipefail

docker_image="registry.gitlab.com/morley-framework/indigo/indigo-trial"

maybe_pull_image() {
    if [[ "$(docker images -q $docker_image 2> /dev/null)" == "" ||
              3600 -le $(($(date '+%s') - $(date '+%s' \
                --date="$(docker inspect -f '{{ .Created }}' $docker_image)")
              ))
        ]];
    then
        docker pull "$docker_image"
    fi
}

if ! docker -v > /dev/null 2>&1 ; then
    echo "Docker does not seem to be installed."
    exit 1
fi


#This functionality is mostly used for testing this script.
if [[ -z "${INDIGO_TRIAL_IMAGE-}" ]];
then
    maybe_pull_image
else
    docker_image="$INDIGO_TRIAL_IMAGE"
fi


if [[ "$#" -eq 0 ]];
then
    docker run --rm "$docker_image" indigo --help
    exit 1
fi

new_command="False"

for arg in "$@"; do
    if [[ $arg == "new" ]]; then
        new_command="True"
    elif [[ $arg == "test" ]]; then
        echo "'./indigo.sh test' command currently not supported."
        exit 1
    fi
done


user_id="$(id -u)"
group_id="$(id -g)"

random_suffix="$(head /dev/urandom | tr -dc A-Za-z0-9 | head -c 10 ; echo '')"
container_name="indigo-container-$random_suffix"

additional_volume=()

if [[ -n "${INDIGO_CACHE_VOLUME-}" ]];
then
    additional_volume=("-v" "$INDIGO_CACHE_VOLUME:/root/.stack")
    docker run --rm -v "$INDIGO_CACHE_VOLUME:/root/.stack" \
           -i "$docker_image" chmod -R go+rwX /root/.stack
fi

if [[ $new_command == "True" ]]; then
    set +e
    docker run --name "$container_name" -i "$docker_image" indigo "$@"
    exit_code="$?"
    set -e
else
    docker_cmd=("export HOME=/root ; cd $(basename "$PWD") && indigo $@")
    set +e
    docker run --user "$user_id":"$group_id" --name "$container_name" \
           --env INDIGO_IN_DOCKER="True" "${additional_volume[@]}" \
           -v "$PWD":/indigo-root/"$(basename "$PWD")" \
           -i "$docker_image" bash -c "${docker_cmd[*]}"
    exit_code="$?"
    set -e
fi

if [ $new_command == "True" ]; then
    docker cp "$container_name":/indigo-root/. .
else
    docker cp "$container_name":/indigo-root/"$(basename "$PWD")"/. .
fi
docker rm "$container_name" 1>/dev/null
exit "$exit_code"
