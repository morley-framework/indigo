#!/bin/bash -e
#
# SPDX-FileCopyrightText: 2020 Tocqueville Group
#
# SPDX-License-Identifier: LicenseRef-MIT-TQ

set -euo pipefail
IFS=$'\n\t'

# Generate a script use to install indigo.
# This script is mean to be run via './scripts/make-indigo.install.sh'.
# This script use a script as a template inside a folder '.scripts/make-indigo-install/install.sh'.
# This script needs access to './snapshots/indigo-snapshot.yaml' and './package.yaml'.
if [ $# -lt 1 ]; then
    echo "Error: Invalid argument."
    echo "Usage: make-indigo-install.sh <output-path>"
    echo "Example: make-indigo-install.sh './tutorial/docs/install.sh'"
    exit 1
fi

# eq: "./tutorial/docs/install.sh"
outputPath=$1
shift

if [ "${1-}" == "" ]; then
  indigo_ver=$(grep '^version:' package.yaml| awk '{print $2}')
  indigo_dep="- indigo-$indigo_ver"
else
  indigo_dep="- git: https://gitlab.com/morley-framework/indigo.git\n  commit: $1"
fi

sed_script="
/^\*\*\*\*indigo$/ {
  r ./snapshots/indigo-snapshot.yaml
  d
}
/^\*\*\*\*dependence$/{
  a $indigo_dep
  d
}
"

sed "$sed_script" ./scripts/make-indigo-install/install.sh > "$outputPath"

chmod +x "$outputPath"
