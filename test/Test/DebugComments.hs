-- SPDX-FileCopyrightText: 2022 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

module Test.DebugComments
       ( whileLeftComments
       , pathFrontendComments
       , pathBackendComments
       , pathAuxComments
       , pathExprComments
       , pathDebugCommentsFullStack
       , whileLeftCommentsFullStack
       , test_Comments
       ) where

import Prelude hiding (drop, swap)

import Hedgehog (Gen)
import Hedgehog.Gen qualified as Gen
import Hedgehog.Range qualified as Range
import Test.Tasty (TestTree)

import Indigo.Compilation
import Lorentz
import Test.Code.Examples (contractWhileLeftIndigo)
import Test.Util

--------------------------------------------------------------------------------
-- Tests
--------------------------------------------------------------------------------

{-# ANN module ("HLint: ignore Reduce duplication" :: Text) #-}

test_Comments :: [TestTree]
test_Comments =
  [ testIndigoContract "Frontend comments"
      genInteger
      genInteger
      (\_ _ _ _ -> pure ())
      (whileLeftComments LogTopLevelFrontendStatements)
      pathFrontendComments

  , testIndigoContract "Backend comments"
      genInteger
      genInteger
      (\_ _ _ _ -> pure ())
      (whileLeftComments LogBackendStatements)
      pathBackendComments

  , testIndigoContract "Aux comments"
      genInteger
      genInteger
      (\_ _ _ _ -> pure ())
      (whileLeftComments LogAuxCode)
      pathAuxComments

  , testIndigoContract "Expression comments"
      genInteger
      genInteger
      (\_ _ _ _ -> pure ())
      (whileLeftComments LogExpressionsComputations)
      pathExprComments
  ]
  where
    genInteger :: Gen Integer
    genInteger = Gen.integral (Range.linearFrom 0 -1000 1000)

whileLeftComments :: CommentsVerbosity -> ContractCode Integer Integer
whileLeftComments lvl =
  compileIndigoContractFull
    (defaultCommentSettings lvl)
    contractWhileLeftIndigo

whileLeftCommentsFullStack :: ContractCode Integer Integer
whileLeftCommentsFullStack =
  compileIndigoContractFull
    (defaultCommentSettings LogTopLevelFrontendStatements) { csPrintFullStackTrace = True }
    contractWhileLeftIndigo

pathFrontendComments :: FilePath
pathFrontendComments = "test/contracts/golden/debug/frontend_comments.tz"

pathBackendComments :: FilePath
pathBackendComments = "test/contracts/golden/debug/backend_comments.tz"

pathAuxComments :: FilePath
pathAuxComments = "test/contracts/golden/debug/aux_comments.tz"

pathExprComments :: FilePath
pathExprComments = "test/contracts/golden/debug/expr_comments.tz"

pathDebugCommentsFullStack :: FilePath
pathDebugCommentsFullStack = "test/contracts/golden/debug/fullstack_comments.tz"
