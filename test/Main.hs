-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

module Main
  ( main
  ) where

import Prelude
import Test.Tasty (defaultMainWithIngredients)

import Test.Cleveland.Ingredients (ourIngredients)
import Test.Util.Golden (regenerateTests)
import Tree (tests)

main :: IO ()
main = tests >>= (defaultMainWithIngredients $ regenerateTests:ourIngredients)
